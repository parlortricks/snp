# Shaw's Nightmare Packer
This program allows you to pack files into an archive. 

Also available on my itch.io at https://parlortricks.itch.io/snp

# Requirements
 - Fennel
 - Lua 5.4

# Howto Run

    fennel snp.fnl [file1] [file2] [file3] ...

# Attributions
Thanks to Mickey Productions for their help. 