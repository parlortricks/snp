meta:
  id: shaws_nightmare_dat
  title: Shaws Nightmare Archive Format
  application: Shaws Nightmare Game
  file-extension: dat
  license: CC0-1.0
  endian: le
doc: |
  Shaws Nightmare is a game created by Mickey Productions and uses its
  own archive format.

  The archive layout is as follows:
  - header
  - file data (just packed together)
  - file index

  I have tested this on Shaws Nightmare 1 & 2

  Written and tested by Parlortricks https://github.com/parlortrickss
doc-ref: https://moddingwiki.shikadi.net/wiki/Shaws_Nightmare_Archive_Format
seq:
  - id: magic 
    contents: [DAT,0x1a]
    doc: archive header, only a valid archive if it has this
  - id: num_items
    type: u2
    doc: number of files contains in archive
  - id: ofs_index
    type: u4
    doc: offset for where the index begins in the file
  - id: len_index
    type: u4
    doc: length of the index
instances:
  index:
    pos: ofs_index  
    size: len_index
    type: index_struct
    doc: index of files located at end of file
types:
  index_struct:
    seq:
      - id: entries
        type: index_entry
        repeat: eos
  index_entry:
    seq:
      - id: name
        size: 16
        type: strz
        doc: file name is null terminated
        encoding: ASCII
      - id: len_body
        type: u4
      - id: ofs_body
        type: u4
    instances:
      body:
        io: _root._io
        pos: ofs_body
        size: len_body
        doc: this contains the raw data of the file