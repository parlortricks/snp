(local fennel (require :fennel))

;; fnlfmt:skip
(local CONF {:_AUTHOR :parlortricks
             :_EMAIL "palortricks@fastmail.fm"
             :_NAME :SNP
             :_VERSION :1.0
             :_URL "https://codeberg.org/parlortricks/snp"
             :_DESCRIPTION "Shaw's Nightmare Packer"
             :_SPDX "MIT License"
             :_SPDXURL "https://spdx.org/licenses/MIT"
             :_LICENSE "    MIT LICENSE
    Copyright 2022 parlortricks <parlortricks@fastmail.fm>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the \"Software\"), to
    deal in the Software without restriction, including without limitation the
    rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
    sell copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS
    OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE."})

;; fnlfnmt: skip
(set CONF._HELP (.. "=====================================================\n"
                    CONF._DESCRIPTION " v" CONF._VERSION "\n"
                    "=====================================================\n"
                    CONF._SPDX " -> " CONF._SPDXURL "\n" "Copyright 2022 "
                    CONF._AUTHOR " <" CONF._EMAIL ">\n\n" CONF._URL "\n"
                    "=====================================================\n"
                    "Usage: fennel snp.fnl [file1] [file2] [file3] ...

  Currently outputs ARCHIVE.DAT, rename to your liking.

  Options:
  -h, --help            Display this help
  -v, --version         Display the version"))

(local archive-magic "DAT\026")

(fn right-pad [s l c]
  (.. s (string.rep (or c 0) (- l (length s)))))

(fn get-filename [path]
  (let [(start finish) (path:find "[%w%s!-={-|]+[_%.].+")]
    (path:sub start (length path))))

(fn exists? [file] ;; Thanks to XeroOI
  (match (os.rename file file)
    (nil _ 13) true
    (?ok ?err) (values ?ok ?err)))

(fn file-list [arg]
  (let [files []]
    (each [_ file (ipairs arg)]
      (if (exists? file)
        (table.insert files file)))
    files))

(fn process-files [data]
  (let [files []]
    (each [_ arg (ipairs data)]
      (let [file {}]
        (with-open [fin (io.open arg :rb)]
          (set file.name (get-filename arg))
          (set file.data (fin:read :*all))
          (set file.size (fin:seek :end)))
        (table.insert files file)))
    files))

(fn pack-files [data]
  (var file-data-offset 0)
  (var file-index-offset 0)
  (var archive-size 0)
  (local archive-header-size 14)
  (local file-header-size 24)
  (each [_ file (ipairs data)]
    (set archive-size (+ archive-size file.size)))
  (local numitems (length data))
  (local index_offset (+ archive-header-size archive-size))
  (local index_size (* (length data) file-header-size) )

  (local index-offset (+ archive-size archive-header-size))
  (with-open [fout (io.open "ARCHIVE.DAT" :wb)]
    (fout:write archive-magic)                      ;; char magic[4];
    (fout:write (string.pack "<i2" numitems))       ;; u16 numitems;
    (fout:write (string.pack "<i4" index_offset))   ;; u32 index_offset;
    (fout:write (string.pack "<i4" index_size))     ;; u32 index_size;

    (set file-data-offset (fout:seek))
    (set file-index-offset index-offset)

    (each [_ file (ipairs data)]
      (fout:seek :set file-data-offset)
      (fout:write file.data)                             ;; u8 data[size] @ offset;
      (set file-data-offset (fout:seek))

      (fout:seek :set file-index-offset)
      (fout:write (right-pad file.name 16 "\0"))         ;; char name[16]; 
      (fout:write (string.pack "<i4" file.size))         ;; u32 size;
      (fout:write (string.pack "<i4" file-data-offset))  ;; u32 offset;
      (set file-index-offset (fout:seek)))))

(if (or (= (length arg) 0) (: (. arg 1) :match :-h) (: (. arg 1) :match :--help))
      (print CONF._HELP)
    (or (: (. arg 1) :match :-v) (: (. arg 1) :match :--version))
      (print (.. CONF._NAME " v" CONF._VERSION))
    (pack-files (process-files (file-list arg))))

